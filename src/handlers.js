const determinePosition = (string, rows, columns) => {
    let rowCoord;
    let colCoord;
    let wall;
    let middle;
    switch (string) {
        case 'top-left':
            rowCoord = 0;
            colCoord = 0;
            wall = 'topWall';
            break;
        case 'top-middle':
            middle = Math.floor(columns / 2);
            rowCoord = 0;
            colCoord = middle;
            wall = 'topWall';
            break;
        case 'top-right':
            rowCoord = 0;
            colCoord = columns - 1;
            wall = 'topWall';
            break;
        case 'left-top':
            rowCoord = 0;
            colCoord = 0;
            wall = 'leftWall';
            break;
        case 'left-middle':
            middle = Math.floor(rows / 2);
            rowCoord = middle;
            colCoord = 0;
            wall = 'leftWall';
            break;
        case 'left-bottom':
            rowCoord = rows - 1;
            colCoord = 0;
            wall = 'leftWall';
            break;
        case 'right-top':
            rowCoord = 0;
            colCoord = columns - 1;
            wall = 'rightWall';
            break;
        case 'right-middle':
            middle = Math.floor(rows / 2);
            rowCoord = middle;
            colCoord = columns - 1;
            wall = 'rightWall';
            break;
        case 'right-bottom':
            rowCoord = rows - 1;
            colCoord = columns - 1;
            wall = 'rightWall';
            break;
        case 'bottom-left':
            rowCoord = rows - 1;
            colCoord = 0;
            wall = 'bottomWall';
            break;
        case 'bottom-middle':
            middle = Math.floor(columns / 2);
            rowCoord = rows - 1;
            colCoord = middle;
            wall = 'bottomWall';
            break;
        case 'bottom-right':
            rowCoord = rows - 1;
            colCoord = columns - 1;
            wall = 'bottomWall';
            break;
        case 'center':
            const middleX = Math.floor(rows / 2);
            const middleY = Math.floor(columns / 2);
            rowCoord = middleX;
            colCoord = middleY;
            wall = 'noWall';
            break;
        
    }
    return {
        row: rowCoord,
        col: colCoord,
        wall: wall
    }
}

const collectGenerateData = () => {
    const cellSize = $('#cell-size-input').val();
    const rows = $('#rows-number-input').val();
    const columns = $('#columns-number-input').val();
    const wallColor = $('#wall-color-input').val();
    const backgroundColor = $('#background-color-input').val();
    const wallThickness = $('#wall-thickness-input').val();
    const entryCellColor = $('#entry-cell-color-input').val();
    const entryCellPosition = determinePosition($('#entry-cell').val(), rows, columns);
    const exitCellColor = $('#exit-cell-color-input').val();
    const exitCellPosition = determinePosition($('#exit-cell').val(), rows, columns);
    if (entryCellPosition === exitCellPosition) {
        alert('Entry and Exit positions must NOT be the same');
        return;
    }
    return {
        cellSize,
        rows,
        columns,
        wallColor,
        backgroundColor,
        wallThickness,
        entryCellColor,
        entryCellPosition,
        exitCellColor,
        exitCellPosition
    }
}
export {
    collectGenerateData,
    determinePosition
}