let maze = document.querySelector('.maze');
let ctx = maze.getContext('2d');

export default class Cell {
    constructor(rowNum, colNum, parentGrid, parentWidth, parentHeight, wallThickness) {
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.parentGrid = parentGrid;
        this.parentWidth = parentWidth;
        this.parentHeight = parentHeight;
        this.wallThickness = +wallThickness;
        this.visited = false;
        this.solved = false;
        this.truePath = false;
        this.walls = {
            topWall: true,
            rightWall: true,
            bottomWall: true,
            leftWall: true,
        };
    }

    solvedNeighbours() {
        let grid = this.parentGrid;
        let row = this.rowNum;
        let col = this.colNum;
        let neighbours = [];

        let top = row !== 0 ? grid[row - 1][col] : undefined;
        let right = col !== grid[row - 1] ? grid[row][col + 1] : undefined;
        let bottom = row !== grid.length - 1 ? grid[row + 1][col] : undefined;
        let left = col !== 0 ? grid[row][col - 1] : undefined;

        if (top && !top.walls.bottomWall && !top.solved) neighbours.push(top);
        if (right && !right.walls.leftWall && !right.solved) neighbours.push(right);
        if (bottom && !bottom.walls.topWall && !bottom.solved) neighbours.push(bottom);
        if (left && !left.walls.rightWall && !left.solved) neighbours.push(left);

        if (neighbours.length !== 0) {
            let random = Math.floor(Math.random() * neighbours.length);
            return neighbours[random];
        } else {
            return undefined;
        }
    }
    checkNeighbours() {
        let grid = this.parentGrid;
        let row = this.rowNum;
        let col = this.colNum;
        let neighbours = [];

        let top = row !== 0 ? grid[row - 1][col] : undefined;
        let right = col !== grid[row - 1] ? grid[row][col + 1] : undefined;
        let bottom = row !== grid.length - 1 ? grid[row + 1][col] : undefined;
        let left = col !== 0 ? grid[row][col - 1] : undefined;

        if (top && !top.visited) neighbours.push(top);
        if (right && !right.visited) neighbours.push(right);
        if (bottom && !bottom.visited) neighbours.push(bottom);
        if (left && !left.visited) neighbours.push(left);

        if (neighbours.length !== 0) {
            let random = Math.floor(Math.random() * neighbours.length);
            return neighbours[random];
        } else {
            return undefined;
        }
    }
    drawTopWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x - offset/2, y);
        ctx.lineTo(x + width / columns + offset/2, y);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    hideTopWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x + offset/2, y);
        ctx.lineTo(x + width / columns - offset/2, y);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    drawRightWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x + width / columns, y - offset/2);
        ctx.lineTo(x + width / columns, y + height / rows + offset/2);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    hideRightWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x + width / columns, y + offset/2);
        ctx.lineTo(x + width / columns, y + height / rows - offset + offset/2);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    drawBottomWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x - offset/2, y + height / rows);
        ctx.lineTo(x + width / columns + offset/2, y + height / rows);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    hideBottomWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x + offset/2, y + height / rows);
        ctx.lineTo(x + width / columns - offset/2, y + height / rows);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    drawLeftWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x, y - offset/2);
        ctx.lineTo(x, y + height / rows + offset/2);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }
    hideLeftWall(x, y, width, height, columns, rows, strokeColor) {
        let offset = this.wallThickness;
        ctx.beginPath();
        ctx.moveTo(x, y + offset/2);
        ctx.lineTo(x, y + height / rows - offset + offset/2);
        ctx.strokeStyle = strokeColor;
        ctx.stroke();
    }

    highlight(rows, columns, entryPosition) {
        let x = this.colNum * this.parentWidth / columns + 1;
        let y = this.rowNum * this.parentHeight / rows + 1;

        if (this.rowNum !== entryPosition.row && this.colNum !== entryPosition.col) {
            ctx.fillStyle = 'green';            
            ctx.fillRect(x, y, this.parentWidth/columns - 3, this.parentHeight/rows - 3);
        }
    }

    drawPathWithLines(rows, columns, bt, direction, solutionLineThickness, backgroundColor, solutionColor) {
        let x = this.colNum * this.parentWidth / columns + this.wallThickness;
        let y = this.rowNum * this.parentHeight / rows + this.wallThickness;
        let centerX = x + this.parentWidth / columns / 2 - this.wallThickness/2;
        let centerY = y + this.parentHeight / rows / 2 - this.wallThickness/2;
        let cellSize = this.parentWidth / columns;
        ctx.lineWidth = solutionLineThickness;

        if (!bt) {
            if (direction === 'hor') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);

                ctx.beginPath();
                ctx.moveTo(x + 2, centerY);
                ctx.lineTo(x + cellSize - 2, centerY);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            } else if (direction === 'ver') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);
                
                ctx.beginPath();
                ctx.moveTo(centerX, y + 2);
                ctx.lineTo(centerX, y + cellSize - 2);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            } else if (direction === 'upRight') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);

                ctx.beginPath();
                ctx.moveTo(centerX, y + cellSize - 2);
                ctx.lineTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.moveTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.lineTo(x + cellSize - 2, y + cellSize/2 - this.wallThickness/2);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            } else if (direction === 'upLeft') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);

                ctx.beginPath();
                ctx.moveTo(centerX, y + cellSize - 2);
                ctx.lineTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.moveTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.lineTo(x + 2, y + cellSize/2 - this.wallThickness/2);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            } else if (direction === 'downRight') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);

                ctx.beginPath();
                ctx.moveTo(centerX, y + 2);
                ctx.lineTo(centerX, centerY);
                ctx.moveTo(centerX, centerY);
                ctx.lineTo(x + cellSize - 2, centerY);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            } else if (direction === 'downLeft') {
                ctx.fillStyle = backgroundColor;
                ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);

                ctx.beginPath();
                ctx.moveTo(centerX, y + 2);
                ctx.lineTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.moveTo(centerX, y + cellSize/2 - this.wallThickness/2);
                ctx.lineTo(x + 2, y + cellSize/2 - this.wallThickness/2);

                ctx.strokeStyle = solutionColor;
                ctx.stroke();
            }
        } else {
            // ctx.fillStyle = 'yellow';
            ctx.fillStyle = backgroundColor;
            ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);
        }
    }

    clear(rows, columns) {
        let x = this.colNum * this.parentWidth / columns + this.wallThickness;
        let y = this.rowNum * this.parentHeight / rows + this.wallThickness;
        ctx.fillStyle = 'black';
        ctx.fillRect(x, y, this.parentWidth/columns - this.wallThickness, this.parentHeight/rows - this.wallThickness);
    }
    removeWalls(cell1, cell2) {
        let x = (cell1.colNum - cell2.colNum);

        if (x === 1) {
            cell1.walls.leftWall = false;
            cell2.walls.rightWall = false;
        } else if (x === -1) {
            cell1.walls.rightWall = false;
            cell2.walls.leftWall = false;
        }

        let y = cell1.rowNum - cell2.rowNum;

        if (y === 1) {
            cell1.walls.topWall = false;
            cell2.walls.bottomWall = false;
        } else if (y === -1) {
            cell1.walls.bottomWall = false;
            cell2.walls.topWall = false;
        }
    }

    show(width, height, rows, columns, entryPosition, exitPosition, wallColor, backgroundColor, entryColor, exitColor){
        let x = (this.colNum * width) / columns + this.wallThickness/2;
        let y = (this.rowNum * height) / rows + this.wallThickness/2;
        let strokeColor;
        ctx.fillStyle = backgroundColor;
        ctx.lineWidth = this.wallThickness;


        if (this.rowNum === entryPosition.row && this.colNum === entryPosition.col) {
            ctx.fillStyle = entryColor;
        }
        if (this.rowNum === exitPosition.row && this.colNum === exitPosition.col) {
            ctx.fillStyle = exitColor;
        }

        if (this.walls.topWall) {
            strokeColor = wallColor;
            this.drawTopWall(x, y, width, height, columns, rows, strokeColor)
        } else {
            strokeColor = backgroundColor;
            this.hideTopWall(x, y, width, height, columns, rows, strokeColor)
        }

        if (this.walls.rightWall) {
            strokeColor = wallColor;
            this.drawRightWall(x, y, width, height, columns, rows, strokeColor)
        } else {
            strokeColor = backgroundColor;
            this.hideRightWall(x, y, width, height, columns, rows, strokeColor)            
        }

        if (this.walls.bottomWall) {
            strokeColor = wallColor;
            this.drawBottomWall(x, y, width, height, columns, rows, strokeColor)
        } else {
            strokeColor = backgroundColor;
            this.hideBottomWall(x, y, width, height, columns, rows, strokeColor)
        }

        if (this.walls.leftWall) {
            strokeColor = wallColor;
            this.drawLeftWall(x, y, width, height, columns, rows, strokeColor)
        } else {
            strokeColor = backgroundColor;
            this.hideLeftWall(x, y, width, height, columns, rows, strokeColor)
        }

        if (this.visited) {
            ctx.fillRect(x + this.wallThickness/2, y + this.wallThickness/2, width / columns - this.wallThickness, height / rows - this.wallThickness);
        }
    }
    
}