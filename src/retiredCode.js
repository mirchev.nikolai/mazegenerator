// drawPathWithArrows method
// origin: cell.js - draws a path using triangular arrows
function drawPathWithArrows(rows, columns,bt, direction) {
    let x = this.colNum * this.parentWidth / columns + 1;
    let y = this.rowNum * this.parentHeight / rows + 1;
    let centerX = x + this.parentWidth / columns / 2;
    let centerY = y + this.parentHeight / rows / 2;
    let cellSize = this.parentWidth / columns;
    ctx.lineWidth = 2;

    if (!bt) {
        if (direction === 'right') {
            ctx.fillStyle = 'black';
            ctx.fillRect(x, y, this.parentWidth/columns - 2, this.parentHeight/rows - 2);

            ctx.beginPath();
            ctx.moveTo(x + cellSize*0.25, y + cellSize*0.25);
            ctx.lineTo(x + cellSize*0.25, y + cellSize*0.75);
            ctx.moveTo(x + cellSize*0.25, y + cellSize*0.75);
            ctx.lineTo(x + cellSize*0.75, centerY);
            ctx.moveTo(x + cellSize*0.75, centerY);
            ctx.lineTo(x + cellSize*0.25, y + cellSize*0.25);

            ctx.strokeStyle = 'green';
            ctx.stroke();
        } else if (direction === 'left') {
            ctx.fillStyle = 'black';
            ctx.fillRect(x, y, this.parentWidth/columns - 2, this.parentHeight/rows - 2);
            
            ctx.beginPath();
            ctx.moveTo(x + cellSize*0.75, y + cellSize*0.25);
            ctx.lineTo(x + cellSize*0.75, y + cellSize*0.75);
            ctx.moveTo(x + cellSize*0.75, y + cellSize*0.75);
            ctx.lineTo(x + cellSize*0.25, centerY);
            ctx.moveTo(x + cellSize*0.25, centerY);
            ctx.lineTo(x + cellSize*0.75, y + cellSize*0.25);

            ctx.strokeStyle = 'green';
            ctx.stroke();
        } else if (direction === 'up') {
            ctx.fillStyle = 'black';
            ctx.fillRect(x, y, this.parentWidth/columns - 2, this.parentHeight/rows - 2);

            ctx.beginPath();
            ctx.moveTo(x + cellSize*0.25, y + cellSize*0.75);
            ctx.lineTo(x + cellSize*0.75, y + cellSize*0.75);
            ctx.moveTo(x + cellSize*0.75, y + cellSize*0.75);
            ctx.lineTo(centerX, y + cellSize*0.25);
            ctx.moveTo(centerX, y + cellSize*0.25);
            ctx.lineTo(x + cellSize*0.25, y + cellSize*0.75);

            ctx.strokeStyle = 'green';
            ctx.stroke();
        } else if (direction === 'down') {
            ctx.fillStyle = 'black';
            ctx.fillRect(x, y, this.parentWidth/columns - 2, this.parentHeight/rows - 2);

            ctx.beginPath();
            ctx.moveTo(x + cellSize*0.25, y + cellSize*0.25);
            ctx.lineTo(x + cellSize*0.75, y + cellSize*0.25);
            ctx.moveTo(x + cellSize*0.75, y + cellSize*0.25);
            ctx.lineTo(centerX, y + cellSize*0.75);
            ctx.moveTo(centerX, y + cellSize*0.75);
            ctx.lineTo(x + cellSize*0.25, y + cellSize*0.25);

            ctx.strokeStyle = 'green';
            ctx.stroke();
        }
    } else {
        ctx.fillStyle = 'black';
        ctx.fillRect(x, y, this.parentWidth/columns - 2, this.parentHeight/rows - 2);
    }
}

// solveWithArrows method
// origin: maze.js - determines the direction of the solving path and invokes the drawPathWithArrows() method
function solveWithArrows(){
    this.solved.solved = true;
    let next = this.solved.solvedNeighbours();
    
    if (next === this.grid[this.exitCellPosition.row][this.exitCellPosition.col]) {
        return;
    }
    
    if (next) {
        let direction;
        if (next.colNum - this.solved.colNum === 1) {
            direction = 'right';
        }
        if (next.colNum - this.solved.colNum === -1) {
            direction = 'left';
        }
        if (next.rowNum - this.solved.rowNum === 1) {
            direction = 'down';
        }
        if (next.rowNum - this.solved.rowNum === -1) {
            direction = 'up';
        }
        this.solved.truePath = true;
        this.stack.push(this.solved);
        this.solved.drawPathWithArrows(this.rows, this.columns, 0, direction);
        this.solved = next;
    } else if (this.stack.length > 0) {
        let cellBack = this.stack.pop();
        this.solved.truePath = false;
        this.solved.drawPathWithArrows(this.rows, this.columns, 1);
        this.solved = cellBack;
    }

    window.requestAnimationFrame(() => {
        this.solveWithArrows();
    });
}