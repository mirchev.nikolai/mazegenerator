import Cell from './cell.js';

let maze = document.querySelector('.maze');

export default class Maze {
    constructor(data) {
        this.width = data.cellSize * data.columns;
        this.height = data.cellSize * data.rows;
        this.rows = data.rows;
        this.columns = data.columns;
        this.wallColor = data.wallColor;
        this.backgroundColor = data.backgroundColor;
        this.wallThickness = data.wallThickness;
        this.entryCellColor = data.entryCellColor;
        this.entryCellPosition = data.entryCellPosition;
        this.exitCellColor = data.exitCellColor;
        this.exitCellPosition = data.exitCellPosition;
        this.grid = [];
        this.stack = [];
        this.solved = null;
        this.current = null;
        this.counter = 0;
    }
    

    setup() {
        for (let r = 0; r < this.rows; r++) {
            let row = [];
            for (let c = 0; c < this.columns; c++) {
                let cell = new Cell(r, c, this.grid, this.width, this.height, this.wallThickness);
                row.push(cell);
            }
            this.grid.push(row);
        }
        
        this.current = this.grid[this.entryCellPosition.row][this.entryCellPosition.col];
        this.solved = this.grid[this.entryCellPosition.row][this.entryCellPosition.col];
        this.current.walls[this.entryCellPosition.wall] = false;
        this.grid[this.exitCellPosition.row][this.exitCellPosition.col].walls[this.exitCellPosition.wall] = false;
    }

    drawVisual() {
        maze.width = this.width + +this.wallThickness;
        maze.height = this.height + +this.wallThickness;
        maze.style.background = this.backgroundColor;
        this.current.visited = true;

        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.columns; c++) {
                let grid = this.grid;
                grid[r][c].show(
                    this.width,
                    this.height,
                    this.rows,
                    this.columns,
                    this.entryCellPosition,
                    this.exitCellPosition,
                    this.wallColor,
                    this.backgroundColor,
                    this.entryCellColor,
                    this.exitCellColor);
            }
        }

        let next = this.current.checkNeighbours();

        if (next) {
            next.visited = true;

            this.stack.push(this.current);

            this.current.highlight(this.rows, this.columns, this.entryCellPosition);

            this.current.removeWalls(this.current, next);
            this.current = next;
        } else if (this.stack.length > 0) {
            let cell = this.stack.pop();
            this.current = cell;
            this.current.highlight(this.rows, this.columns, this.entryCellPosition);
        }

        if (this.stack.length === 0) {
            return;
        }
        window.requestAnimationFrame(() => {
            this.drawVisual();
        });
    }
    drawFast() {
        maze.width = this.width + +this.wallThickness;
        maze.height = this.height + +this.wallThickness;
        maze.style.background = this.backgroundColor;
        this.current.visited = true;
        this.counter++;

        let next = this.current.checkNeighbours();

        if (next) {
            next.visited = true;

            this.stack.push(this.current);

            this.current.removeWalls(this.current, next);
            this.current = next;
        } else if (this.stack.length > 0) {
            let cell = this.stack.pop();
            this.current = cell;
        }

        if (this.stack.length === 0) {
            for (let r = 0; r < this.rows; r++) {
                for (let c = 0; c < this.columns; c++) {
                    let grid = this.grid;
                    grid[r][c].show(
                        this.width,
                        this.height,
                        this.rows,
                        this.columns,
                        this.entryCellPosition,
                        this.exitCellPosition,
                        this.wallColor,
                        this.backgroundColor,
                        this.entryCellColor,
                        this.exitCellColor);
                }
            }
            this.counter = 0;
            return;
        }

        if (this.counter > 5000) {
            this.counter = 0;
            setTimeout(() => {
                this.drawFast();
            }, 80);
        } else {
            this.drawFast();
        }
    }

    solveWithLines(previousCell) {
        this.counter++;
        this.solved.solved = true;
        let next = this.solved.solvedNeighbours();
        let prevCell;
        let prev;

        if (this.solved === this.grid[this.entryCellPosition.row][this.entryCellPosition.col]) {
            prev = this.solved;
        } else {
            prev = previousCell;
        }
        
        if (this.solved === this.grid[this.exitCellPosition.row][this.exitCellPosition.col]) {
            this.counter = 0;
            return;
        }
        
        let direction;
        if (next) {
            prevCell = this.solved;
            if (prev.colNum - this.solved.colNum === 0) {  // if prev and current are in the same column
                if (prev.rowNum - this.solved.rowNum === -1) {
                    // going down in the same column
                    if (next.colNum - this.solved.colNum === -1) {
                        direction = 'downLeft';
                    } else if (next.colNum - this.solved.colNum === 1) {
                        direction = 'downRight';
                    } else {
                        direction = 'ver';
                    }
                } else if (prev.rowNum - this.solved.rowNum === 1) {
                    // going UP in the same column
                    if (next.colNum - this.solved.colNum === -1) {
                        direction = 'upLeft';
                    } else if (next.colNum - this.solved.colNum === 1) {
                        direction = 'upRight';
                    } else {
                        direction = 'ver';
                    }
                    
                }
            } else if (prev.colNum - this.solved.colNum === -1) { // if it's going RIGHT in the same row
                if (next.rowNum - this.solved.rowNum === -1) {
                    direction = 'downLeft';
                } else if (next.rowNum - this.solved.rowNum === 1) {
                    direction = 'upLeft';
                } else {
                    direction = 'hor';
                }
            } else if (prev.colNum - this.solved.colNum === 1) {  // if it's going LEFT in the same row
                if (next.rowNum - this.solved.rowNum === -1) {
                    direction = 'downRight';
                } else if (next.rowNum - this.solved.rowNum === 1) {
                    direction = 'upRight';
                } else {
                    direction = 'hor';
                }
            }
            
            this.solved.truePath = true;
            this.stack.push(this.solved);
            this.solved.drawPathWithLines(this.rows, this.columns, 0, direction, 1, this.backgroundColor, 'green');
            this.solved = next;
        } else if (this.stack.length > 0) {
            let cellBack = this.stack.pop();
            prevCell = this.stack[this.stack.length - 1];
            this.solved.truePath = false;
            this.solved.show(
                this.width,
                this.height,
                this.rows,
                this.columns,
                this.entryCellPosition,
                this.exitCellPosition,
                this.wallColor,
                this.backgroundColor,
                this.entryCellColor,
                this.exitCellColor);
            this.solved.drawPathWithLines(this.rows, this.columns, 1, direction, 2, this.backgroundColor, 'green');
            this.solved = cellBack;
        }

        if (this.counter > 5000) {
            this.counter = 0;
            setTimeout(() => {
                this.solveWithLines(prevCell);
            }, 80);
        } else {
            this.solveWithLines(prevCell);
        }
    }

    resetSolvePaths() {
        for (let r = 0; r < this.rows; r++) {
            for (let c = 0; c < this.columns; c++) {
                let grid = this.grid;
                grid[r][c].solved = false;
                grid[r][c].truePath = false;
                grid[r][c].clear(this.rows, this.columns);
                grid[r][c].show(
                    this.width,
                    this.height,
                    this.rows,
                    this.columns,
                    this.entryCellPosition,
                    this.exitCellPosition,
                    this.wallColor,
                    this.backgroundColor,
                    this.entryCellColor,
                    this.exitCellColor);
            }
        }
        this.solved = this.grid[this.entryCellPosition.row][this.entryCellPosition.col];
    }
}