import Maze from './src/maze.js';
import {
    generateMazeVisual,
    generateMazeFast
} from './src/events.js';

import {
    collectGenerateData
} from './src/handlers.js';

let maze;

generateMazeVisual((e) => {
    e.preventDefault();
    const generateData = collectGenerateData();
    maze = new Maze(generateData);
    maze.setup();
    maze.drawVisual();
});
generateMazeFast((e) => {
    e.preventDefault();
    const generateData = collectGenerateData();
    maze = new Maze(generateData);
    maze.setup();
    maze.drawFast();
});
solveWithLinesButton.addEventListener('click', () => {
    maze.solveWithLines();
});
clearSolutionButton.addEventListener('click', () => {
    maze.resetSolvePaths();
});